<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
			<h1 align="center">用户列表</h1>
			<table width="800" height="30" align="center" border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td>编号</td>
					<td>用户名</td>
					<td>密码</td>
					<td>创建时间</td>
					<td>操作</td>
				</tr>
				<c:if test="${empty account }">
					出错啦！！！
				</c:if>
				<c:if test="${not empty account }">
				<c:forEach items="${account}" var="accounts">
				<tr>
					<td>${accounts.id }</td>
					<td>${accounts.account}</td>
					<td>${accounts.password }</td>
					<td>${accounts.crateat }</td>
					<td>
						<a href="${pageContext.request.contextPath}/students/${accounts.id }/update">修改</a>
						<a class="btn_destroy" href="${pageContext.request.contextPath}/students/${accounts.id }/del">删除</a>
					</td>
				</tr>
				</c:forEach>
					<tr >
					<td colspan="5" align="center">
						<a href="${pageContext.request.contextPath}/students/add">添加用户</a>
					</td>
					</tr>
				</c:if>
			</table>
	<script type="text/javascript" src="${pageContext.request.contextPath }/assets/lib/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/assets/app/student.js"></script>
</body>
</html>