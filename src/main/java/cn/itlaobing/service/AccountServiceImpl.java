package cn.itlaobing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itlaobing.entity.Accounts;
import cn.itlaobing.mapper.AccountsMapper;

@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	private AccountsMapper accountMapper;
	
	@Transactional
	@Override
	public Accounts save(Accounts accounts) {
		accountMapper.insert(accounts);
		return accounts;
	}
	@Transactional(readOnly=true)
	@Override
	public Accounts findById(Integer id) {
		return accountMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly=true)
	@Override
	public List<Accounts> findAll() {
		return accountMapper.findAll();
	}
	@Transactional
	@Override
	public Accounts updateById(Accounts accounts) {
		accountMapper.updateByPrimaryKey(accounts);
		return accounts;
	}
	@Transactional
	@Override
	public void del(Integer id) {
	   accountMapper.deleteByPrimaryKey(id);
		
	}

}
