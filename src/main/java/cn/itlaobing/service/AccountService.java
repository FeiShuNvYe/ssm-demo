package cn.itlaobing.service;

import java.util.List;

import cn.itlaobing.entity.Accounts;

public interface AccountService {
	public Accounts save(Accounts accounts);
	public Accounts  findById(Integer id);
	public List<Accounts> findAll();
	public Accounts updateById(Accounts accounts);
	public void del(Integer id);
}
